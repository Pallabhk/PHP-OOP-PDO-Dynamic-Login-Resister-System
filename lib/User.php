<?php
	include_once 'Session.php';
	include 'Database.php';

	Class User{

		private $db;
		public function __construct(){
			
			$this->db = new Database();
		}
		public function userRegistration($data){

			$name       =$data['name'];
			$username   =$data['username'];
			$email      =$data['email'];
			$password   =md5($data['pass']);

			$chk_email  =$this->emailCheck($email);

			if ($name == "" or $username == "" or $email == "" or $password == "") {
				
				$msg = "<div class='alert alert-danger'><strong>Error....!</strong>Field must not be Empty</div>";

				return $msg;
			}
			if(strlen($username) <3){
				$msg = "<div class='alert alert-danger'><strong>Error....!</strong>USer name is too Srort.</div>";

				return $msg;
			}elseif(preg_match('/[^a-z0-9_-]/i',$username)){
				$msg = "<div class='alert alert-danger'><strong>Error....!</strong>Ueer name must be alphabet,numeric,desh,underscore!</div>";

				return $msg;
			}
			if(filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE){
				$msg = "<div class='alert alert-danger'><strong>Error....!</strong>The Email adress is invalite</div>";

				return $msg;
			}
			if($chk_email == true){
				$msg = "<div class='alert alert-danger'><strong>Error....!</strong>The Email adress allready Exist</div>";

				return $msg;
			}
			$sql = "INSERT INTO tbl_user(username,pass,name,email) VALUES( :username, :pass, :name, :email)";
			$query	=$this->db->pdo->prepare($sql);
			$query->bindValue(':username',$username);
			$query->bindValue(':name',    $name);
			$query->bindValue(':email',$email);
			$query->bindValue(':pass',$password);
			$result = $query->execute();
				if($result){
					$msg = "<div class='alert alert-success'><strong>SuccessFull....!</strong>You have resister Successfully</div>";

				return $msg;
				}else{
					$msg = "<div class='alert alert-danger'><strong>Sorry....!</strong>There are some problem to insert your details..</div>";

				return $msg;
				}
		}
		//Email check Method
		public function emailCheck($email){
			$sql ="SELECT email  FROM tbl_user WHERE email= :email ";

			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':email',$email);
			$query->execute();
			if ($query->rowCount() >0 ) {
				return true;
			}else{
				return false;
			}
		}

		//Loh in user 

		public function getLoinUser($email, $password){
			$sql ="SELECT *  FROM tbl_user WHERE email= :email AND pass= :pass LIMIT 1 ";

			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':email',$email);
			$query->bindValue(':pass',$password);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);
			return $result;
		}
		//Login method .....
		public function userLogin($data){
			$email      =$data['email'];
			$password   = md5($data['password']);

			$chk_email  =$this->emailCheck($email);

			if ( $email == "" or $password == "") {
				
				$msg = "<div class='alert alert-danger'><strong>Error....!</strong>Field must not be Empty</div>";

				return $msg;
			}
			if(filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE){
				$msg = "<div class='alert alert-danger'><strong>Error....!</strong>The Email adress is invalite</div>";

				return $msg;
			}
			if($chk_email == false){
				$msg = "<div class='alert alert-danger'><strong>Error....!</strong>The Email adress not Exist</div>";

				return $msg;
			}
				

			//Session method

			$result =$this->getLoinUser($email, $password);
			if($result){
					Session::init();
					Session::set("login",true);
					Session::set("id",$result->id);
					Session::set("name",$result->name);
					Session::set("username",$result->username);
					Session::set("loginmsg", "<div class='alert alert-success'><strong>SuccessFull</strong>You are Loggedin</div>");

					header("Location:index.php");

				}else{
					$msg = "<div class='alert alert-danger'><strong>Error....!</strong>Data not found.</div>";

				return $msg;	
			}
		}
		public function getUserData(){
			$sql ="SELECT *  FROM tbl_user ORDER BY id DESC";

			$query = $this->db->pdo->prepare($sql);
			$query->execute();
			$result = $query->fetchAll();
			return $result;
		}

		public function getUserById($userid){
			$sql ="SELECT *  FROM tbl_user WHERE id = :id LIMIT 1";

			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':id',$userid);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);
			return $result;
		}
	}
?>