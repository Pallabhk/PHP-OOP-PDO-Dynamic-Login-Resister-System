<?php
    include 'lib/User.php';
	include 'inc/header.php';
	Session::checkSession();
?>

<?php

	$user = new User();
		if (isset($_GET['id'])) {
			$userid =(int)$_GET['id'];
		}
?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>User Profile <span class="pull-right"><a class="btn btn-primary" href="index.php">Back</a></span></h2>
			</div>
			<div class="panel-body">
				<div style="max-width: 600px; margin:0 auto;">

					<?php
						$userdata = $user->getUserById($userid);

						if ($userdata) {?>
						<form action="" method="POST">
						<div class="form-group">
							<label for="name">Name:</label>
							<input type="text" name="name" class="form-control" value="<?php echo $userdata->name;?>">
						</div>
						<div class="form-group">
							<label for="username">Username:</label>
							<input type="text" name="username" class="form-control"  value="<?php echo $userdata->username;?>">
						</div>
						<div class="form-group">
							<label for="email">Email Address:</label>
							<input type="text" name="email" class="form-control"  value="<?php echo $userdata->email;?>">
						</div>

						<?php
						$secId =Session::get("id");
							if ($userid ==$secId) {?>
								<button type="submit" name="update" class="btn btn-success">Update</button>
								
						<?php	} ?>
					
					</form>	 
					<?php	} ?>
					
				</div>
			</div>
		</div>
	<?php include 'inc/footer.php';?>