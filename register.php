<?php
	include 'inc/header.php';
	include 'lib/User.php';
?>

<?php
	$user = new User();

	if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])){

		$userReg =$user->userRegistration($_POST);
	}
?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>User Registration</h2>
			</div>
			<div class="panel-body">
				<div style="max-width: 600px; margin:0 auto;">
					<?php
						if(isset($userReg)){
							echo $userReg;
						}
					?>

					<form action="" method="POST">
						<div class="form-group">
							<label for="name">Name:</label>
							<input type="text" name="name" class="form-control">
						</div>
						<div class="form-group">
							<label for="username">Username:</label>
							<input type="text" name="username" class="form-control">
						</div>
						<div class="form-group">
							<label for="email">Email Address:</label>
							<input type="text" name="email" class="form-control">
						</div>
						<div class="form-group">
							<label for="password">Email Address:</label>
							<input type="password" name="pass" class="form-control">
						</div>
						<button type="submit" name="register" class="btn btn-success">Submit</button>
						<button type="reset" name="reset" class="btn btn-primary">Reset</button>
					</form>
				</div>
			</div>
		</div>
	<?php include 'inc/footer.php';?>