<?php
	include_once '/../lib/Session.php';
	Session::init();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login Registration System</title>

	<!--Bootstrap CSS -->
	<link rel="stylesheet" href="inc/assets/css/bootstrap.min.css">
	<!--Main CSS -->
	<link rel="stylesheet" href="inc/assets/css/bootstrap.min.css">
	<script src="inc/assets/js/bootstrap.min.js"></script>
	<script src="inc/assets/js/jquery-1.12.4.js"></script>
</head>
<?php
	if (isset($_GET['action']) && $_GET['action'] == "logout") {
		Session::destroy();
	}
?>
<body>
	<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php">Login Resister System OOP & PDO</a>
				</div>
				<ul class="nav navbar-nav pull-right">
					<?php
						$id = Session::get("id");
						$userlogin =Session::get("login");
							if ($userlogin == true) {?>
					<li><a href="profile.php">Profile</a></li>
					<li><a href="?action=logout">Log out</a></li>

					<?php }else{?>
					<li><a href="login.php">Log in</a></li>
					<li><a href="register.php">Register</a></li>
					
					<?php }?>
					
					
				</ul>
			</div>
		</nav>