	<?php
	    include 'lib/User.php';
		include 'inc/header.php';
		
		Session::checkSession();
		$user = new User();
	?>
	<?php
		$loginmsg = Session::get("loginmsg");
		if ($loginmsg) {
			echo  $loginmsg;
		}
		Session::set("loginmsg",NULL);
	?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>USer List <span class="pull-right">Wellcome<strong>
				<?php
					$name = Session::get("username");
						if (isset($name)) {
							echo $name;
						}
				?>
				</strong></span></h2>
			</div>
			<div class="panel-body">
				<table class="table table-striped">
					<th width="20%">Serial</th>
					<th width="20%">Name</th>
					<th width="20%">Username</th>
					<th width="20%">Email</th>
					<th width="20%">Action</th>

					<?php

						$user = new User();
						
						$userdata =$user->getUserData();
						if ($userdata) {
							$id = 0;
							foreach ($userdata as  $value) {
								$id++;?>
					<tr>
						<td><?php echo $id;?></td>
						<td><?php echo $value['name']; ?></td>
						<td><?php echo $value['username']; ?></td>
						<td><?php echo $value['email']; ?></td>
						<td>
							<a class="btn btn-primary" href="profile.php?id=<?php echo $id;?>">View</a>
						</td>
					</tr>
					<?php	} } ?>
					
				</table>
			</div>
		</div>
	<?php include 'inc/footer.php';?>